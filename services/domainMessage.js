const Config = require('../config/ibmConfig');
const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');

class domainMessage {
    constructor() {
        this.init = this.init.bind(this);
        this.getDomainSession = this.getDomainSession.bind(this);
        this.postMessage = this.postMessage.bind(this);
    }

    // Authenticate
    init(skill) {
        const service = new AssistantV2({
            version: Config[skill].version,
            authenticator: new IamAuthenticator({
                apikey: Config[skill].apikey,
            }),
            url: Config[skill].url,
        });
        return service;
    }

    // Get domain session data
    async getDomainSession(skill) {
        return await this.init(skill).createSession({
            assistantId: Config[skill].assistantId
        });
    };

    // Post message to Domain Skill and get response
    async postMessage(msg, sessionID, skill) {
        return await this.init(skill).message({
            assistantId: Config[skill].assistantId,
            sessionId: sessionID,
            input: {
                "message_type": "text",
                "text": msg
            }
        });
    };
}

module.exports = new domainMessage();