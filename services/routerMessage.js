const Config = require('../config/ibmConfig');
const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');

const idMapSkill = {};

class routerMessage {
    constructor() {
        this.init = this.init.bind(this);
        this.getRouterSession = this.getRouterSession.bind(this);
        this.getSkillObject = this.getSkillObject.bind(this);
    }

    // Authenticate
    init() {
        const service = new AssistantV2({
            version: Config["router skill"].version,
            authenticator: new IamAuthenticator({
                apikey: Config["router skill"].apikey,
            }),
            url: Config["router skill"].url,
        });
        return service;
    }

    // Get router session data
    async getRouterSession() {
        return await this.init().createSession({
            assistantId: Config["router skill"].assistantId
        });
    }

    // Post message to Router Skill and get skill id
    async getSkillObject(msg, routerSession) {
        return await this.init().message({
            assistantId: Config["router skill"].assistantId,
            sessionId: routerSession,
            input: {
                "message_type": "text",
                "text": msg
            }
        });
    }

    // Delete router session
    async deleteRouterSession(routerSession) {
        return await this.init().deleteSession({
            assistantId: Config["router skill"].assistantId,
            sessionId: routerSession
        });
    }

    async setSkill(id, skill) {
        idMapSkill[id] = skill;
        console.log('This is setSkill: ', idMapSkill[id]);
    }

    async getSkill(id) {
        console.log('This is getSkill: ', idMapSkill[id]);
        return idMapSkill[id]
    }
}

module.exports = new routerMessage();