const express = require('express');
const bodyParser = require('body-parser');
const Config = require('./config/envConfig');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));

// Setup template engine
app.set('view engine', 'ejs');
app.set('views', 'views');

// Routes
const botRoutes = require('./routes/callMessage');

app.use('/', botRoutes);
app.listen(Config.port);

module.exports = app;