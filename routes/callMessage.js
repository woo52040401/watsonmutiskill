const express = require('express');
const session = require('express-session');
const routerController = require('../controllers/routerController');
const logger = require('../controllers/logger');

const router = express.Router();
const app = express();

app.use(session({
    secret: 'sessionTest',
    resave: true,
    saveUninitialized: true
}));

logger.init('watson');

router.post('/myMessage', async function (req, res, next) {
    let message = req.body.text;
    let httpSession = req.body.session;
    let response = await routerController.inRouter(httpSession, message);
    res.json(response);
});

module.exports = router;
