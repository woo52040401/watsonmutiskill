const routerMessage = require('../services/routerMessage');
const domainController = require('./domainController');

const idMapSkill = {};

class routerController {

    async inRouter(id, message) {

        let routerSession;

        // If skill exist
        let skill = routerMessage.getSkill(id);
        console.log(skill);
        console.log('Step 1 : in router contro id: ', id);

        if (skill) {
            console.log('--------------');
            return await domainController.inDomain(id, message, skill);
        }

        // Get Router Session
        let getRouterSession = await routerMessage.getRouterSession();
        routerSession = getRouterSession.result.session_id;
        console.log('This is new router session: ', routerSession);

        // Get Skill ID
        let routerAns = await routerMessage.getSkillObject(message, routerSession);
        let routerContext = JSON.parse(routerAns.result.output.generic[0].text);
        console.log('This is new router session: ', routerContext);

        // Keep skill
        let newSkill = routerContext.skill_id;
        routerMessage.setSkill(id, newSkill);
        idMapSkill[id] = newSkill;

        return await domainController.inDomain(id, message, newSkill);
    }
}

module.exports = new routerController();