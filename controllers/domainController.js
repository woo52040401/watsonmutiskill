const domainMessage = require('../services/domainMessage');

const idMapSession = {};

class domainController {

    async inDomain(id, message, skill) {

        // If session delete yet
        if (id in idMapSession) {
            return await domainMessage.postMessage(message, idMapSession[id], skill);
        }

        let domainSession;

        // Get Domain Session & keep it
        let getDomainSession = await domainMessage.getDomainSession(skill);
        domainSession = getDomainSession.result.session_id;
        idMapSession[id] = domainSession;

        // Get Answer
        let domainAns = await domainMessage.postMessage(message, idMapSession[id], skill);
        logger.log("The user say: ", message);
        let domainContext = domainAns.result.output.generic[0].text;
        logger.log("The response: ", domainContext);

        return domainContext;
    }

}

module.exports = new domainController();